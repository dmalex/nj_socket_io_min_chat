const express = require("express")
const app = express()
app.get('/', function(req, res){
  res.send('<h1>B.RF Group - Simple Socket.IO Chat Server by Dmitry Alexandrov</h1>')
})
const server = require("http").createServer(app)
const io = require('socket.io')(server);
const PORT = 3000

io.on("connection", socket => {
    console.log("A user is connected.");

    socket.on("message", msg => {
        console.log("Message: ", msg)
        io.emit("message", msg) // broadcast received message
    })

    socket.on('disconnect', function(){
      console.log('A user is disconnected.')
    })
})

const showLog = () => {
    console.log("Simple Socket.IO Chat Server is running on port " + PORT)
}
server.listen(PORT, showLog)
